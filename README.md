# Beispiel-Setup für die TypeScript-Nutzung

## Vorbereiten der Arbeitsumgebung

Da wir im Folgenden einigermassen vorbildliche und zeitgemäße Formen der
JavaScript bzw. eben TypeScript-Nutzung verwenden möchten, sind leider einige
Vorbereitungen nötig, um vernünftige Arbeitsbedingungen zu schaffen.

Auch wenn ich weiter unten erklären werde, wie man die meisten hier
beschriebenen Vorbereitungschritte dadurch abkürzen kann, dass man einfach
vorbereitetes Setup per GIT klont, sollten zumindest die elementaren Schritte
skizziert werden. 

### Installieren von `node` und `yarn`

Am verwendeden Rechner sollten zumindest die ganze elementaren Werzeuge
[node](https://nodejs.org) und [yarn](https://yarnpkg.com/) installiert sein.

Das gestaltet sich leider unter debian Linux (dort nennt sich ersteres Packerl:
`nodejs`) ziemlich unbefriedigend, weil man in dem Fall unbedingt die Version
aus dem `experimental` Zweig installieren muss, um wenigstens eine einigermaßen
aktuelle bzw. kompatible Ausgabe nutzen zu können. Aber auch dieser Ausgabe
fehlen leider wichtige Bestandteile (konkret der `npm`-Packetmanager und die
`npx` Befehlsausführung). Auch `yarn`, das man heranziehen kann, um diese
debian-Einschränkungen mit einem ohnehin deutlich benutzerfreundlichen Ersatz
für die JavaScript-Packetverwaltung zu umgehen, wurde unter debian in `yarnpg`
umbenannt, was zu einiger Verwirrung führen kann, obwohl eben nur statt `yarn`
ständig `yarnpkg` tippen muss, oder eben ein entsprechendes alias in der
verwendeten shell definiert.

also ungefähr:

    apt install -t experimental nodejs
    apt install -t testing yarnpkg
    echo "alias yarn=yarnpkg" >> ~/.bashrc && source ~/.bashrc

### Editor und dortige TypeScript-Unterstützung installieren

Gute Unterstützung im Editor ist enorm hilfreiche. Ich würde deshalb unbedingt
empfehlen einen Editor mit guter TypeScript-Unterstützung zu installieren.
Speziell [VSCode](https://code.visualstudio.com/) ist in dieser Hinsicht
wirklich sehr empfehlenswert. Zur Not können aber evtl. auch Lösungen wie
[GitPod](https://www.gitpod.io/) oder [Che](https://www.eclipse.org/che/)
ähnliches leisten.

### Einrichtung der Arbeitsumgebung (from scratch)

Neues Verzeichnis anlegen und initialisieren:

    mkdir simple-client
    cd simple-client

    yarn init

Ausfüllen zumindest der Felder:

    question name (simple-client): 
    question version (1.0.0): 
    question description: Einfacher GraphQL Client
    question entry point (index.js): index.ts
    ...

### Hinzufügen und konfigurieren weiterer Entwicklungswerkzeuge

    Da wir statt urtümlichem JavaScript besser von Anfang an [TypeScript](https://www.typescriptlang.org/) verwenden wollen, und die Resultate in zeitgemäße ES6-Module packen wollen, was mit Hilfe von [rollup](https://rollupjs.org/) und einigen seiner plugins recht gut abgewickelt werden kann, sind auch noch diese zusätzlichen Entwicklungsabhängigkeiten zu installieren. 

    yarn add -D typescript tslib terser\
       rollup rollup-plugin-typescript \
       rollup-plugin-commonjs \
       rollup-plugin-node-resolve \
       rollup-plugin-terser \
       rollup-plugin-serve \
       rollup-plugin-livereload

Typscript-Umgebung initialisieren und konfigurieren:

    npx tsc --init

Im damit angelegten `tsconfig.json`-File sollten zumindest folgende Änderungen vorgenommen
werden:

```
    "target": "es2017",
    "module": "ESNext",
    "outDir": "dist",
```
Auch `rollup` benötigt noch das Konfigurationsfile `rollup.config.js`:

```javascript
import typescript from 'rollup-plugin-typescript'
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import { terser } from 'rollup-plugin-terser';
import serve from 'rollup-plugin-serve';
import livereload from 'rollup-plugin-livereload';

export default {
    plugins: [
        typescript(),
        resolve(),
        commonjs(),
        serve({open: true, contentBase: ''}), // öffnet die webpage gleich im browser
        livereload()
    ],
    input: 'src/index.ts',
    output: [
        { 
            file: 'dist/bundle-es.js', 
            format: 'es', 
            sourcemap: true 
        },
        { 
            file: 'dist/bundle-min-es.js', 
            format: 'es', 
            sourcemap: true,
            plugins: [terser()]
        },
    ]
}
```

## Ein erster Test

Anlegen des Source-Verzeichnisses und einem Minimum an Testcode in `src/index.ts`:

    mkdir src

```javascript
console.log("Hallo Graz");
```

Anlegen eines minimalistischen HTML-Testfiles `index.html`, um das modul im
Browser testen zu können:

```html
<html>
    <head>
        <title>testpage</title>
        <meta charset="utf-8"/>
    </head>
    <body>
        <h1>test</h1>
        <script type="module">
            import "./dist/bundle-es.js";
        </script>
    </body>
</html>
```

Im `rollup`-Packer wurde als letztes Plugin `serve` installiert, so dass die
gepackten Ergebnisse gleich in einem integrierten Webserver getestet werden
können. Das macht vor allem im Zusammenspiel mit der Option `--watch` Sinn, wo
`rollup` dann im Hintergrund bei Änderungen im Source Code gleich automatisch
alles ständig neu übersetzt und packt, so dass man nur den Code editieren muss
und die Änderungen sofort nach dem Speichern im Browser ihre Wirkung entfalten:

    npx rollup -c --watch

Mit `CTRL+C` kann man diesen Prozess auch wieder stoppen.

In der Entwicklerkonsole des Browsers (`F12` drücken!), sollte man nun die
Ausgabe sehen.

Die generierte Module kann man übrigens auch außerhalb des Browsers mit `node`
oder [`deno`](https://deno.land/) testen bzw. auszuführen:

    node dist/bundle-es.js

oder:

    deno dist/bundle-es.js

## Abkürzung der Installationsprozedur durch Verwendung von GIT bzw. GitLab

Um diese Umständlichen Vorbereitungsschritte nicht alle händisch abwickelen zu müssen, hab ich ein  elementares Skelett des beschriebenen Setups auf GitLab platziert, um es sehr einfach klonen zu können.

In dem Fall sind also lokal als Vorbereitung nur `node`, `yarn[pkg]`, ein
geeigneter Editor und `git` nötig.

Bevor man das Repositority auf den lokalen Rechner klont, sollte man in der grafischen GitLab-Oberfläche das Projekt https://gitlab.com/mash-graz/typescript-base besser [`forken`](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork), wenn man eigene Änderungen später möglichst einfach dort in einer eignen Kopie nutzen kann. Dieser Schritt ist aber nicht unbedingt nötig, wenn man das Beispiel-Setup nur lokal nutzen will.

Das Synchronisieren bzw. Herunterladen erfolgt wie gewohnt mit dem befehl, der einem nach drücken des `clone` Buttons angezeigt wird.

bspw.:
    git clone https://gitlab.com/mash-graz/typescript-base.git

In der lokalen Kopie, die nach dem Klonen am lokalen Rechner zu finden ist, muss man nun den Befehl `yarn` einmal aufrufen, damit die notwendigen JavaScript Werkzeuge heruntergeladen werden.

also:

    yarn
    yarn rollup -c --watch

Es sollte sich ein Browser-Fenster öffenen, wo der Beispielcode aus `src/index.ts` zu hören ist.

Im diesem File ist auch auch noch zweites auskommentiertes winziges Beispiel enthalten, das [tone.js](https://tonejs.github.io/) zur Umsetzung nutzt.

Um es zu starten, muss man einfach nur den anderen Code löschen od. auskommentieren, und statt dessen die `//` vor dem betreffenden Zweizeiler entfernen.
Da Tone.js sich auf eine externe TypeScript Library stützt, muss auch die vor Verwendung einmal in das Projekt einmalig installiert werden. Das erledigt man mit:

    yarn add tone

danach kann man es wieder in gewohnter Weise starten:

    yarn rollup -c --watch

Im Setup ist auch eine vorkonfigurierte `.gitlab-ci.yml` Datei enthalten, die
dafür sorgt, dass auf GitLab, sobald Änderungen reinkommen, also mit `git
commit` und `git push` an den Server übertragen wurden, dort automatisch der
Code übersetzt wird und gemeinsam mit der Testseite via
[GitLab-Pages](https://docs.gitlab.com/ee/user/project/pages/index.html) im Web
zugänglich gemacht wird. So können die Resulate sehr einfach mit anderen geteilt
und direkt im Browser ausprobiert werden.

in dem Fall hier mit dem URL: https://mash-graz.gitlab.io/typescript-base/
