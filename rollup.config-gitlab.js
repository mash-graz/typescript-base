import typescript from 'rollup-plugin-typescript'
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import { terser } from 'rollup-plugin-terser';

export default {
    plugins: [
        typescript(),
        resolve(),
        commonjs(),
    ],
    input: 'src/index.ts',
    output: [
        { 
            file: 'dist/bundle-es.js', 
            format: 'es', 
            sourcemap: true 
        },
        { 
            file: 'dist/bundle-min-es.js', 
            format: 'es', 
            sourcemap: true,
            plugins: [terser()]
        },
    ]
}
