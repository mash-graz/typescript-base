// // Tone.js Beispiel
// import Tone from "tone";
// const tone_osc = new Tone.Oscillator(220).toMaster().start();


// Minimalistisches WebAudio-Beispiel
const ctx = new AudioContext();
const osc = ctx.createOscillator();
const vol = ctx.createGain();
osc.frequency.value = 440;
vol.gain.value=0.25;
osc.connect(vol)
vol.connect(ctx.destination);
osc.start();
